import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '@app/_models';
import {Router} from '@angular/router';
import {AuthenticationService} from '@app/_services/api';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'angular78-jwt-boilerplate';
  currentUser: User;

  private _UNSUBSCRIBE: Subscription = new Subscription();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {
    this.getCurrentUser();
  }

  ngOnDestroy(): void {
    this._UNSUBSCRIBE.unsubscribe();
  }

  private getCurrentUser() {
    this._UNSUBSCRIBE.add(
      this.authenticationService.currentUser.subscribe(user => this.currentUser = user)
    );
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
