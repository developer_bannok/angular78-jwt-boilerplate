import { Component, OnInit } from '@angular/core';
import {CartService} from '@app/_services/shared/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  cart: any;
  fileUrl;

  constructor(private cartService: CartService) { }

  ngOnInit() {
  }

  getCartService() {
    this.cart = this.cartService.getCart();
  }

  addToCart() {
    this.cart = this.cartService.addToCart();
  }

  emptyCart() {
    this.cartService.clearCart();
  }

}
