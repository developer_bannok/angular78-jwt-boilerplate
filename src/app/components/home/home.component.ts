import { Component, OnInit } from '@angular/core';
import {UserService} from '@app/_services/api';
import {User} from '@app/_models';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loading = false;
  users: User[];
  items: { name: string; id: number }[];
  pageOfItems: Array<any>;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loading = true;
    this.getUserAll();
    this.getDataTables();
  }

  private getUserAll() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.loading = false;
      this.users = users;
    });
  }

  private getDataTables() {
    this.items = Array(150).fill(0).map((x, i) => ({ id: (i + 1), name: `Item ${i + 1}`}));
  }

  onChangePage(pageOfItems: Array<any>) {
    this.pageOfItems = pageOfItems;
  }

}
